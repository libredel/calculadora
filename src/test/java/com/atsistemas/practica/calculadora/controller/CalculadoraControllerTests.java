package com.atsistemas.practica.calculadora.controller;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

@RunWith(SpringRunner.class)
@WebMvcTest(CalculadoraController.class)
public class CalculadoraControllerTests {

	@Autowired
    private MockMvc mockMvc;
	
	@Test
    public void testAdicionNumerosEnteros() throws Exception {
    	mockMvc.perform(get("/calculadora/v1.0/sumar/1/2"))
    	       .andDo(print())
    	       .andExpect(status().isOk())    	       
    	       .andExpect(jsonPath("$.resultado", is(3)));    	          	
    }
	
	@Test
    public void testSustraccionNumerosEnteros() throws Exception {
    	mockMvc.perform(get("/calculadora/v1.0/restar/1/2"))
    	       .andDo(print())
    	       .andExpect(status().isOk())    	       
    	       .andExpect(jsonPath("$.resultado", is(-1)));    	          	
    }
    
}
