package com.atsistemas.practica.calculadora.config;

import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class WebApplicationConfig implements WebMvcConfigurer {

	/**
	 * Configuracion de Swagger Calculadora API versión 1.0.
	 *
	 * @return configuración swagger en un objeto Docket.
	 */
	@Bean
	public Docket swaggerCalculadoraApi10() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("calculadora-api-1.0").select()
				.apis(RequestHandlerSelectors.basePackage("com.atsistemas.practica.calculadora.controller"))
				.paths(PathSelectors.regex("/calculadora/v1.0.*")).build().apiInfo(new ApiInfoBuilder().version("1.0").title("Calculadora API")
						.description("Calculadora API v1.0").build());
	}

	/**
	 * Configuracion de Swagger Calculadora API versión 2.0.
	 *
	 * @return configuración swagger en un objeto Docket.
	 */
	@Bean
	public Docket swaggerCalculadoraApi20() {
		return new Docket(DocumentationType.SWAGGER_2).groupName("calculadora-api-2.0").select()
				.apis(RequestHandlerSelectors.basePackage("com.atsistemas.practica.calculadora.controller"))
				.paths(PathSelectors.regex("/calculadora/v2.0.*")).build().apiInfo(new ApiInfoBuilder().version("2.0").title("Calculadora API")
						.description("Calculadora API v2.0").build());
	}

	/**
	 * Información para la portada de la documentación Swagger.
	 *
	 * @return un objeto apinfo.
	 */
	private ApiInfo apiInfo() {
		return new ApiInfo("Documentación API REST Calculadora",
				"Servicios RESTFul para la interacción con la calculadora.", "1.0",
				"http://url_a_terminos_del_servicio/service.html",
				new Contact("José Manuel Librero Delgado", "http://www.linkedin.com/in/jose-manuel-librero-delgado",
						"libredel@gmail.com"),
				"Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0", Collections.emptyList());
	}

}
