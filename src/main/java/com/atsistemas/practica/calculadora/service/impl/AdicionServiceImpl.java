package com.atsistemas.practica.calculadora.service.impl;

import org.springframework.stereotype.Service;

import com.atsistemas.practica.calculadora.service.OperacionService;

import io.corp.calculator.TracerImpl;

@Service("adicionService")
public class AdicionServiceImpl implements OperacionService {

	TracerImpl tracer = new TracerImpl();
	
	/**
	 * Aplica una operación aritmética de adición entre dos números.
	 *
	 * @param numero1
	 * @param numero2
	 * @return long
	 */
	@Override
	public long aplicar(long numero1, long numero2) {
		tracer.trace(
				"Adición aplicada a (numero1: " + numero1 + ", numero2: " + numero2 + ")");
		return (numero1 + numero2);
	}

	/**
	 * Verifica el operador de una operación aritmética de adición.
	 *
	 * @param operador
	 * @return boolean
	 */
	@Override
	public boolean validar(char operador) {
		tracer.trace(
				"Validación operador de adición (operador: " + operador + ")");
		return operador == '+' ? true : false;
	}

}
