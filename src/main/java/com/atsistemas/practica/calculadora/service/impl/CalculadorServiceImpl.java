package com.atsistemas.practica.calculadora.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import com.atsistemas.practica.calculadora.dto.ResultadoOperacionDto;
import com.atsistemas.practica.calculadora.service.CalculadorService;
import com.atsistemas.practica.calculadora.service.OperacionService;

import io.corp.calculator.TracerImpl;

@Service
public class CalculadorServiceImpl implements CalculadorService {

	@Autowired
	@Qualifier("adicionService")
	private OperacionService adicionService;

	@Autowired
	@Qualifier("sustraccionService")
	private OperacionService sustraccionService;

	TracerImpl tracer = new TracerImpl();

	/**
	 * Realiza el cálculo una operación aritmética entre dos números según el
	 * operador.
	 *
	 * @param numero1
	 * @param numero2
	 * @param operador
	 * @return ResultadoOperacionDto
	 */
	@Override
	public ResultadoOperacionDto calcular(long numero1, long numero2, char operador) {
		tracer.trace(
				"Se realiza cálculo(numero1: " + numero1 + ", numero2: " + numero2 + ", operador: " + operador + ")");
		ResultadoOperacionDto resultadoDto = new ResultadoOperacionDto();

		switch (operador) {
		case '+':
			resultadoDto.setResultado(adicionService.aplicar(numero1, numero2));
			break;
		case '-':
			resultadoDto.setResultado(sustraccionService.aplicar(numero1, numero2));
			break;
		default:
			tracer.trace(
					"Error de cálculo(numero1: " + numero1 + ", numero2:" + numero2 + ", operador: " + operador + ")");
			throw new IllegalArgumentException("Operación no válida para el operador : " + operador);
		}

		tracer.trace("Resultado de cálculo(numero1: " + numero1 + ", numero2:" + numero2 + ", operador: " + operador
				+ ") = " + resultadoDto.getResultado());
		return resultadoDto;

	}

}
