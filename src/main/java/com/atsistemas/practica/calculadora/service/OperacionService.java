package com.atsistemas.practica.calculadora.service;

public interface OperacionService {
	
	/**
	 * Aplica una operación aritmética entre dos números.
	 *
	 * @param numero1
	 * @param numero2
	 * @return long
	 */
	long aplicar(long numero1, long numero2);
	
	/**
	 * Verifica el operador de una operación aritmética.
	 *
	 * @param operador
	 * @return boolean
	 */
	boolean validar(char operador);
}
