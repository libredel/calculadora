package com.atsistemas.practica.calculadora.service;

import com.atsistemas.practica.calculadora.dto.ResultadoOperacionDto;

public interface CalculadorService {
	
	/**
	 * Realiza el cálculo una operación aritmética entre dos números según el operador.
	 *
	 * @param numero1
	 * @param numero2
	 * @param operador
	 * @return ResultadoOperacionDto
	 */
	public ResultadoOperacionDto calcular(long numero1, long numero2, char operador);
}
