package com.atsistemas.practica.calculadora.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constantes {
	public final char OPERADOR_ADICION = '+';
	public final char OPERADOR_SUSTRACION = '-';
}
