package com.atsistemas.practica.calculadora.controller;

import javax.validation.constraints.Digits;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.atsistemas.practica.calculadora.dto.ResultadoOperacionDto;
import com.atsistemas.practica.calculadora.service.CalculadorService;
import com.atsistemas.practica.calculadora.util.Constantes;

import io.corp.calculator.TracerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value = "/calculadora")
@RestController
@Validated
@RequestMapping("/calculadora")
public class CalculadoraController {

	@Autowired
	private CalculadorService calculadorService;
	
	TracerImpl tracer = new TracerImpl();
	
	/**
	 * Devuelve el resultado de una operación aritmética de adición entre dos número.
	 *
	 * @param numero1
	 * @param numero2
	 * @return ResultadoOperacionDto
	 */
	@ApiOperation(value = "Devuelve el resultado de una operación aritmética de adición entre dos número.", 
			response = ResultadoOperacionDto.class,
			notes = "Devuelve el resultado de una operación aritmética de adición entre dos número.")
	@GetMapping({"/v1.0/sumar/{numero1}/{numero2}", "/v2.0/sumar/{numero1}/{numero2}"})
	public ResponseEntity<ResultadoOperacionDto> sumar(
			@PathVariable(name = "numero1") @Digits(fraction = 2, integer = Integer.MAX_VALUE) Long numero1,
			@PathVariable(name = "numero2") @Digits(fraction = 2, integer = Integer.MAX_VALUE) Long numero2) {
		tracer.trace("Se realiza llamada a endpoint SUMAR: calculadora/v1.0/sumar/" + numero1 + "/" + numero2);
		return ResponseEntity.ok().body(this.calculadorService.calcular(numero1, numero2, Constantes.OPERADOR_ADICION));
	}
	
	/**
	 * Devuelve el resultado de una operación aritmética de sustracción entre dos número.
	 *
	 * @param numero1
	 * @param numero2
	 * @return ResultadoOperacionDto
	 */
	@ApiOperation(value = "Devuelve el resultado de una operación aritmética de sustracción entre dos número.", 
			response = ResultadoOperacionDto.class,
			notes = "Devuelve el resultado de una operación aritmética de adición entre dos número.")
	@GetMapping({"/v1.0/restar/{numero1}/{numero2}", "/v2.0/restar/{numero1}/{numero2}"})
	public ResponseEntity<ResultadoOperacionDto> restar(
			@PathVariable(name = "numero1") @Digits(fraction = 2, integer = Integer.MAX_VALUE) Long numero1,
			@PathVariable(name = "numero2") @Digits(fraction = 2, integer = Integer.MAX_VALUE) Long numero2) {
		tracer.trace("Se realiza llamada a endpoint RESTAR: calculadora/v1.0/restar/" + numero1 + "/" + numero2);
		return ResponseEntity.ok().body(this.calculadorService.calcular(numero1, numero2, Constantes.OPERADOR_SUSTRACION));
	}
	
	/**
	 * Devuelve el resultado de una operación aritmética de multiplicación entre dos número.
	 *
	 * @param numero1
	 * @param numero2
	 * @return ResultadoOperacionDto
	 */
	@ApiOperation(value = "Devuelve el resultado de una operación aritmética de multiplicación entre dos número.", 
			response = ResultadoOperacionDto.class,
			notes = "Devuelve el resultado de una operación aritmética de multiplicación entre dos número.")
	@GetMapping({"/v2.0/multiplicar/{numero1}/{numero2}"})
	public ResponseEntity<ResultadoOperacionDto> multiplicar(@PathVariable(name = "numero1") Long numero1,
			@PathVariable(name = "numero2") Long numero2) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Devuelve el resultado de una operación aritmética de multiplicación entre dos número.
	 *
	 * @param numero1
	 * @param numero2
	 * @return ResultadoOperacionDto
	 */
	@ApiOperation(value = "Devuelve el resultado de una operación aritmética de división entre dos número.", 
			response = ResultadoOperacionDto.class,
			notes = "Devuelve el resultado de una operación aritmética de división entre dos número.")
	@GetMapping({"/v2.0/dividir/{numero1}/{numero2}"})
	public ResponseEntity<ResultadoOperacionDto> dividir(@PathVariable(name = "numero1") Long numero1,
			@PathVariable(name = "numero2") Long numero2) {
		// TODO Auto-generated method stub
		return null;
	}
}
