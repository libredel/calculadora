
# Spring Boot Calculadora API! :rocket:

Microservicio para realizar operaciones aritméticas. En la versión 1.0 existen dos endpoints para sumar y restar, y en la versión 2.0 dos endpoints más para multiplicar y dividir (por instrucciones del enunciado de la práctica están sin implementar).

Calculadora API v1.0

    /calculadora/v1.0/sumar/{numero1}/{numero2}
    /calculadora/v1.0/restar/{numero1}/{numero2}
     
Calculadora API v2.0
 
	/calculadora/v2.0/restar/{numero1}/{numero2} 
	/calculadora/v2.0/sumar/{numero1}/{numero2}
	/calculadora/v2.0/multiplicar/{numero1}/{numero2} 
	/calculadora/v2.0/dividir/{numero1}/{numero2}
	

Una vez levantado el microservicio, con la [ui de swagger](http://localhost:8080/swagger-ui.html) podrá realizar verificaciones de estos endpoints (http://localhost:8080/swagger-ui.html)

## Instrucciones

Ejecución de la aplicación:

	mvn spring-boot:run

Ejecución de tests unitarios: 

	mvn test  
	
Ejecución de tests de integración: 
	
	mvn verify  
  
Creación del jar: 
	
	mvn clean package

Ejecución del .jar generado
	
	java -jar target/calculadora.jar
